# AES Implementation in TASM (32 bit)

This is an implementation of the Advanced Encryption Standard (a specification upon the more general Rijndael encryption standard), also known as FIPS 197. It is written in Borland's Turbo Assembler language, and meant to be compiled in a DOS environment.

## Building

Please make sure you have the `TASM` and `TLINK` utilities in your `PATH`, then execute the `COMP.BAT` script by typing `comp` into your DOS shell. The script should compile both versions of the program, and report errors if any are present.

## Usage

The building process results in two binaries: `AES-128` and `AES-256`, both operate their namesake algorithms. This program uses Cipher Block Chaining with the Ciphertext Stealing scheme of CBC-CS2. This means that in case where the length of data can't be split perfectly into blocks, the last incomplete block is encrypted and inserted before the second-to-last block. Keep that in mind if comparing output to other AES progams.

Please ensure the files containing the Initialisation Vector, key, and data to be encrypted/decrypted are present in your current directory before executing one of two binaries. During execution, the program will ask to specify the names of the input files and the name of a final output file, where the results of the process will be stored. If any error occurs while the program is running, it will report such and exit immediately.
