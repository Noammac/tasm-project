; main.asm
; Main file, to be executed when calling the program.

jumps ; Handle near/far jumps without intervention

ifdef VER
    if VER EQ 256
        Nk   EQU 8
        Nr   EQU 14
        VERT EQU <'256'>
    elseif VER EQ 128
        Nk   EQU 4
        Nr   EQU 10
        VERT EQU <'128'>
    endif
else
    display "Error: Please provide an AES version (128 or 256 bit) via TASM"
    display "       flags, either /dVER=256 or /dVER=128."
    err     "No AES version provided."
endif

ideal
smart ; Will make things optimised, only enable once you're done debugging.
P386
model small

include "procs.inc"
include "imacros.mac"

stack

dataseg

; SECTION 1: MESSAGES AND UI CONSTANTS {{{
; Welcome message
welcome db "Welcome to an AES-",VERT," implementation in TASM, written by Noam Barnea.",13,10,"$"

; Choosing operation method
choice  db 13,10,"Please choose the operation method, [e]ncryption or [d]ecryption:",13,10,"$"
cherr   db 13,10,"Invalid choice, try again.",13,10,"$"

; Asking for filenames
ivquer  db 13,10,"Please enter the name of the file containing the Initialization Vector (IV):",13,10,"$"
keyquer db "Please enter the name of the file containing the ",VERT," bit key:",13,10,"$"
flequer db 13,10,"Please enter the name of the file containing the data you want to convert:",13,10,"$"
resquer db 13,10,"Please enter the name of the file in which you want to store conversion results",13,10
        db "(note that if the file already exists its content WILL BE OVERWRITTEN)",13,10,"$"
goodbye db 13,10,"Done writing to file. Thank you for using the program.",13,10,"$"

; Messages for file errors
ferr    db 13,10,"A file error occured, terminating program. Error message: $"
err2    db "File not found",13,10,"$"
err3    db "Path not found",13,10,"$"
err4    db "Too many open files",13,10,"$"
err5    db "Access denied",13,10,"$"
err6    db "Invalid handle",13,10,"$"
err12   db "Invalid access",13,10,"$"
errs    dw 11 dup(?) ; Will act as an error table.
; END SECTION 1: MESSAGES AND UI CONSTANTS }}}

; SECTION 2: ALGORITHM CONSTANTS AND TABLES {{{
; The S-Box and the reverse S-Box, calculated via the python code in /gen
sbox     db 063h,07Ch,077h,07Bh,0F2h,06Bh,06Fh,0C5h,030h,001h,067h,02Bh,0FEh,0D7h,0ABh,076h
         db 0CAh,082h,0C9h,07Dh,0FAh,059h,047h,0F0h,0ADh,0D4h,0A2h,0AFh,09Ch,0A4h,072h,0C0h
         db 0B7h,0FDh,093h,026h,036h,03Fh,0F7h,0CCh,034h,0A5h,0E5h,0F1h,071h,0D8h,031h,015h
         db 004h,0C7h,023h,0C3h,018h,096h,005h,09Ah,007h,012h,080h,0E2h,0EBh,027h,0B2h,075h
         db 009h,083h,02Ch,01Ah,01Bh,06Eh,05Ah,0A0h,052h,03Bh,0D6h,0B3h,029h,0E3h,02Fh,084h
         db 053h,0D1h,000h,0EDh,020h,0FCh,0B1h,05Bh,06Ah,0CBh,0BEh,039h,04Ah,04Ch,058h,0CFh
         db 0D0h,0EFh,0AAh,0FBh,043h,04Dh,033h,085h,045h,0F9h,002h,07Fh,050h,03Ch,09Fh,0A8h
         db 051h,0A3h,040h,08Fh,092h,09Dh,038h,0F5h,0BCh,0B6h,0DAh,021h,010h,0FFh,0F3h,0D2h
         db 0CDh,00Ch,013h,0ECh,05Fh,097h,044h,017h,0C4h,0A7h,07Eh,03Dh,064h,05Dh,019h,073h
         db 060h,081h,04Fh,0DCh,022h,02Ah,090h,088h,046h,0EEh,0B8h,014h,0DEh,05Eh,00Bh,0DBh
         db 0E0h,032h,03Ah,00Ah,049h,006h,024h,05Ch,0C2h,0D3h,0ACh,062h,091h,095h,0E4h,079h
         db 0E7h,0C8h,037h,06Dh,08Dh,0D5h,04Eh,0A9h,06Ch,056h,0F4h,0EAh,065h,07Ah,0AEh,008h
         db 0BAh,078h,025h,02Eh,01Ch,0A6h,0B4h,0C6h,0E8h,0DDh,074h,01Fh,04Bh,0BDh,08Bh,08Ah
         db 070h,03Eh,0B5h,066h,048h,003h,0F6h,00Eh,061h,035h,057h,0B9h,086h,0C1h,01Dh,09Eh
         db 0E1h,0F8h,098h,011h,069h,0D9h,08Eh,094h,09Bh,01Eh,087h,0E9h,0CEh,055h,028h,0DFh
         db 08Ch,0A1h,089h,00Dh,0BFh,0E6h,042h,068h,041h,099h,02Dh,00Fh,0B0h,054h,0BBh,016h

rsbox    db 052h,009h,06Ah,0D5h,030h,036h,0A5h,038h,0BFh,040h,0A3h,09Eh,081h,0F3h,0D7h,0FBh
         db 07Ch,0E3h,039h,082h,09Bh,02Fh,0FFh,087h,034h,08Eh,043h,044h,0C4h,0DEh,0E9h,0CBh
         db 054h,07Bh,094h,032h,0A6h,0C2h,023h,03Dh,0EEh,04Ch,095h,00Bh,042h,0FAh,0C3h,04Eh
         db 008h,02Eh,0A1h,066h,028h,0D9h,024h,0B2h,076h,05Bh,0A2h,049h,06Dh,08Bh,0D1h,025h
         db 072h,0F8h,0F6h,064h,086h,068h,098h,016h,0D4h,0A4h,05Ch,0CCh,05Dh,065h,0B6h,092h
         db 06Ch,070h,048h,050h,0FDh,0EDh,0B9h,0DAh,05Eh,015h,046h,057h,0A7h,08Dh,09Dh,084h
         db 090h,0D8h,0ABh,000h,08Ch,0BCh,0D3h,00Ah,0F7h,0E4h,058h,005h,0B8h,0B3h,045h,006h
         db 0D0h,02Ch,01Eh,08Fh,0CAh,03Fh,00Fh,002h,0C1h,0AFh,0BDh,003h,001h,013h,08Ah,06Bh
         db 03Ah,091h,011h,041h,04Fh,067h,0DCh,0EAh,097h,0F2h,0CFh,0CEh,0F0h,0B4h,0E6h,073h
         db 096h,0ACh,074h,022h,0E7h,0ADh,035h,085h,0E2h,0F9h,037h,0E8h,01Ch,075h,0DFh,06Eh
         db 047h,0F1h,01Ah,071h,01Dh,029h,0C5h,089h,06Fh,0B7h,062h,00Eh,0AAh,018h,0BEh,01Bh
         db 0FCh,056h,03Eh,04Bh,0C6h,0D2h,079h,020h,09Ah,0DBh,0C0h,0FEh,078h,0CDh,05Ah,0F4h
         db 01Fh,0DDh,0A8h,033h,088h,007h,0C7h,031h,0B1h,012h,010h,059h,027h,080h,0ECh,05Fh
         db 060h,051h,07Fh,0A9h,019h,0B5h,04Ah,00Dh,02Dh,0E5h,07Ah,09Fh,093h,0C9h,09Ch,0EFh
         db 0A0h,0E0h,03Bh,04Dh,0AEh,02Ah,0F5h,0B0h,0C8h,0EBh,0BBh,03Ch,083h,053h,099h,061h
         db 017h,02Bh,004h,07Eh,0BAh,077h,0D6h,026h,0E1h,069h,014h,063h,055h,021h,00Ch,07Dh

; Round Constants, basically powers of two. It's more quick to use an array than implement xdiv
rcon     db 08Dh,001h,002h,004h,008h,010h,020h,040h,080h,01Bh,036h

; The polynomials by which to multiply for the MixColumn step
mixcols  dd 03010102h
rmixcols dd 0B0D090Eh
; END SECTION 2: ALGORITHM CONSTANTS AND TABLE }}}

; SECTION 3: VARIABLES AND FLAGS {{{
; Two buffers for filenames, we're going to be handling two files at the same time at most.
flname1  db 13, 14 dup(?)
flname2  db 13, 14 dup(?)

; Two buffers for file handles. Same here.
handle1  dw ?
handle2  dw ?

; The Cipher Key and the Expanded Key (which is a list of Round Keys)
key      db 4*Nk               ; Key block has Nk columns and 4 rows, always.
rndkeys  db 4*Nb*(Nr+1) dup(?) ; There need to be Nb*(Nr+1) round keys, each of length 4.

; BEGIN Constants that change in the case of decryption
decflag  dw 0                  ; Are we encrypting or decrypting? Also used for ShiftRow and is therefore
                               ; word-size when it could've been a byte. Stacks are bad like that.
sboxaddr dw offset sbox        ; Which S-Box to use
polyaddr dw offset mixcols     ; What polynomial to multiply by in MixColumn
ekeyaddr dw offset rndkeys     ; The address from which to begin traversing the expanded key
delta    dw 4*Nb               ; How to traverse the expanded key
fullblks dw ?                  ; How many full blocks there are in the file
remaindr dw ?                  ; The length of the final block
; END   Constants that change in the case of decryption 

prevstat db 4*Nb dup(?)        ; The previous state (or the IV), needed for CBC.
state    db 4*Nb dup(?)        ; The intermediate state, 128 bits, always Nb cols and 4 rows.

inter    db 4 dup(?)           ; Somewhere to store intermediate polynomial multiplication results
; END SECTION 3: VARIABLES AND FLAGS }}}

codeseg

start:
    startupcode
    ; Set up the error table
    mov  [errs],    offset err2
    mov  [errs+2],  offset err3
    mov  [errs+4],  offset err4
    mov  [errs+6],  offset err5
    mov  [errs+8],  offset err6
    mov  [errs+20], offset err12

    printStr    <offset welcome>
    printStr    <offset keyquer>
    getFilename <offset flname1>
    openFile    0 <offset flname1+2> <offset handle1>
    readFile    <offset handle1> 4*Nk <offset key>
    ; If the file includes less than 4*Nk bytes, it'll be zero-padded during key expansion and encryption
    ; If it includes more, only the first 4*Nk bytes will be read and used.
    closeFile   <offset handle1>

    printStr    <offset ivquer>
    getFilename <offset flname1>
    openFile    0 <offset flname1+2> <offset handle1>
    readFile    <offset handle1> 4*Nb <offset prevstat>
    ; Same goes here, you could content in the IV file that's shorter or longer than a block, it's still
    ; valid. The program doesn't care.
    closeFile   <offset handle1>

    push offset key
    push [ekeyaddr]
    push offset sbox
    push offset rcon
    call KeyExpansion

    method_choice_loop:
    printStr    <offset choice>
    getChar
    cmp  al, 'e'
    je   cont
    cmp  al, 'd'
    je   decrypt
    jmp  method_choice_loop

    decrypt:
    mov  [sboxaddr], offset rsbox
    mov  [polyaddr], offset rmixcols
    neg  [delta]
    mov  [decflag],  2
    add  [ekeyaddr], Nr*4*Nb
    mov  si,         [ekeyaddr]
    mov  cx,         Nr-1

    inv_expansion_loop:
    sub  si, 4*Nb
    push si
    push offset rmixcols
    push offset inter
    call MixColumn
    dec  cx
    jnz  inv_expansion_loop

    cont:
    printStr    <offset flequer>
    getFilename <offset flname1>
    openFile    0 <offset flname1+2> <offset handle1>
    getSize     <offset handle1>
    mov  cx,         4*Nb
    div  cx
    mov  [fullblks], ax
    mov  [remaindr], dx
    seekAbs     <offset handle1> 0 0

    printStr    <offset resquer>
    getFilename <offset flname2>
    createFile  <offset flname2+2> <offset handle2>

    cmp  [remaindr], 0
    je   block_loop
    ; Business as usual if we don't have to steal anything, don't pass on ANY BLOCK.
    dec  [fullblks]
    je   ciphertext_stealing
    ; If there is one full block and one partial block, don't bother with the loop,
    ; just do the funky swaps.
    
    block_loop:
    readFile    <offset handle1> 4*Nb <offset state>

    cmp  [decflag], 0
    jne  encrypt_decrypt
    push offset state
    push offset prevstat
    call AddRoundKey ; In encryption: XOR previous ciphertext or IV with current plaintext

    encrypt_decrypt:
    call Rounds

    cmp  [decflag], 0
    je   block_loop_write
    push offset state
    push offset prevstat
    call AddRoundKey ; In decryption: XOR previous ciphertext or IV with deciphered plaintext

    block_loop_write:
    writeFile   <offset handle2> 4*Nb <offset state>
    cmp  [decflag], 0
    jne  decrypt_cbc
    replace_state
    ; In encryption: move the new ciphertext into the previous ciphertext before encrypting a new block
    jmp end_block_loop

    decrypt_cbc:
    ; Since we need the ciphertext of the block we just decrypted, we're just going to read it from
    ; file again. I can't see a better way which doesn't waste memory if encrypting.
    getPos      <offset handle1>
    sub  ax, 4*Nb
    sbb  dx, 0
    seekAbs     <offset handle1> dx ax
    readFile    <offset handle1> 4*Nb <offset prevstat>

    end_block_loop:
    dec  [fullblks]
    jg   block_loop
    ; This means that if there's one partial block we run the loop once, but if there's a standard
    ; amount it runs for the specified amount of blocks.
    
    cmp  [remaindr], 0
    je   exit ; Nothing left to do
    cmp  [fullblks], -2
    je   exit ; We encrypted one partial block

    ciphertext_stealing:
    ; CBC-CS2
    ; If we're here then:
    ; a) We've already encrypted all the full blocks but one, and now must do the Ciphertext Stealing
    ;    operations on the two blocks that remain.
    ; b) There are only two blocks, one partial, one not. We're going to do the Ciphertext Stealing on
    ;    the both of them, XORing the first one with the IV.
    readFile    <offset handle1> 4*Nb <offset state>
    cmp  [decflag], 0
    jne  cts_decrypt
    
    push offset state
    push offset prevstat
    call AddRoundKey
    call Rounds

    replace_state
    
    mov  [dword ptr state],    0
    mov  [dword ptr state+4],  0
    mov  [dword ptr state+8],  0
    mov  [dword ptr state+12], 0
    
    readFile    <offset handle1> [remaindr] <offset state>
    
    push offset state
    push offset prevstat
    call AddRoundKey
    call Rounds
    
    writeFile   <offset handle2> 4*Nb <offset state>
    writeFile   <offset handle2> [remaindr] <offset prevstat>
    jmp  exit

    cts_decrypt:
    call Rounds

    readFile    <offset handle1> [remaindr] <offset state>
    ; We have now constructed C_{n-1}

    call Rounds
    push offset state
    push offset prevstat
    call AddRoundKey

    writeFile   <offset handle2> 4*Nb <offset state>

    getPos      <offset handle1>
    sub  ax, 4*Nb
    sbb  dx, 0
    sub  ax, [remaindr]
    sbb  dx, 0
    seekAbs     <offset handle1> dx ax

    readFile    <offset handle1> 4*Nb <offset state>
    readFile    <offset handle1> [remaindr] <offset prevstat>

    call Rounds
    push offset state
    push offset prevstat
    call AddRoundKey

    writeFile   <offset handle2> [remaindr] <offset state>

exit:
    closeFile   <offset handle1>
    closeFile   <offset handle2>
    printStr    <offset goodbye>
    exitcode 0

file_error:
    sub ax, 2
    shl ax, 1
    mov si, ax
    mov bx, offset errs
    printStr <offset ferr>
    printStr [bx+si]
    exitcode 1

proc Rounds
    ; Go through all the rounds of AES encryption/decryption.
    ; That means the initial round, the middle rounds, and the final round.
    ; DOES NOT IMPLEMENT CBS TO ALLOW FOR USE IN CIPHERTEXT STEALING
    ; This function has to be defined here to allow for the passing of parameters through memory.
    ;
    ; Input
    ;   decflag  - 0 if in encryption mode, 2 if in decryption, also the shift param for ShiftRow.
    ;   sboxaddr - address of the S-Box to use in ByteSub.
    ;   polyaddr - address of the polynomial to use for MixColumn.
    ;   ekeyaddr - address of the side of the Expanded Key to start from (end or beginning),
    ;              used for AddRoundKey.
    ;   delta    - how to go through the Expanded Key, backwards or forwards.
    ; Output
    ;   state    - state is now fully decrypted or encrypted
    ; Calling Convention
    ;   Irrelevant
    ; Registers Modified
    ;   bx, cx

    mov  bx, [ekeyaddr]
    push offset state
    push bx
    call AddRoundKey

    mov  cx, Nr-1
    add  bx, [delta]

    cipher_loop:
    ; STANDARD ENCRYPTION/DECRYPTION STEPS {{{
    push offset state
    push [sboxaddr]
    call ByteSub

    push offset state
    push [decflag]
    call ShiftRow

    push offset state
    push [polyaddr]
    push offset inter
    call MixColumn

    push offset state
    push bx
    call AddRoundKey

    add  bx, [delta]
    dec  cx
    jnz  cipher_loop

    push offset state
    push [sboxaddr]
    call ByteSub

    push offset state
    push [decflag]
    call ShiftRow

    push offset state
    push bx
    call AddRoundKey
    ; END STANDARD ENCRYPTION/DECRYPTION STEPS }}}

    ret
endp Rounds

end start
