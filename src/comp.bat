echo off
:: comp.bat
:: Compile all sources appropriately
tasm /t /dVER=128 procs,procs128 ; /t /dVER=128 main,aes-128
if not errorlevel 1 tlink aes-128+procs128
tasm /t /dVER=256 procs,procs256 ; /t /dVER=256 main,aes-256
if not errorlevel 1 tlink aes-256+procs256
