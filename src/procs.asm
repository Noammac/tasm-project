; procs.asm
; Procedures to be used in the main program.
; Compile *before* main.

ifdef VER
    if VER EQ 256
        Nk EQU 8
        Nr EQU 14
    elseif VER EQ 128
        Nk EQU 4
        Nr EQU 10
    endif
else
    display "Error: Please provide an AES version (128 or 256 bit) via TASM"
    display "       flags, either /dVER=256 or /dVER=128."
    err     "No AES version provided."
endif

ideal
smart ; Will make things optimised, only enable once you're done debugging.
P386
model small

include "procs.inc"
include "imacros.mac"

codeseg

proc xtime
    ; Get a polynomial in GF(2^8) represented by a byte-sized number
    ; and multiply it by the polynomial x.
    ;
    ; Ex: xtime(57h) = xtime(x^6+x^4+x^2+x+1) = x^7+x^5+x^3+x^2+x = 0AEh;
    ;     xtime(0AEh) = xtime(x^7+x^5+x^3+x^2+x) = x^8+x^6+x^4+x^3+x^2 (this is outside the bounds
    ;     of a byte, therefore it needs to reduced) XOR x^8+x^4+x^3+x+1 = x^6+x^2+x+1 = 47h;
    ;
    ; Input
    ;   num - the number itself
    ; Output
    ;   dx  - the result of the multiplication
    ; Calling Convention
    ;   Irrelevant
    ; Registers Modified
    ;   dx

    arg  num:word
    push bp
    mov  bp, sp

    mov  dx, [num]
    shl  dx, 1
    cmp  dh, 1
    jne  SHORT xtime_exit
    xor  dx, 11Bh

    xtime_exit:
    pop  bp
    ret  2
endp xtime

proc gmul
    ; Get two polynomials in GF(2^8) represented by bytes and multiply them according to the field
    ; rules. This procedure uses xtime iterative for the sake of simplicity, as GF(2^8) multiplication
    ; is distributive over addition (per the requirements of such a field operation).
    ;
    ; Input
    ;   ml1 - first multiplication argument
    ;   ml2 - second multiplication argument
    ; Output
    ;   dx  - byte-sized result of multiplication
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   dx

    arg  ml2,ml1
    push bp
    mov  bp, sp
    push 0

    gmul_loop_start:
    mov  dx, 1
    and  dx, [ml2]
    jz   SHORT gmul_loop_end
    mov  dx, [ml1]
    xor  [bp-2], dx

    gmul_loop_end:
    push [ml1]
    call xtime
    mov  [ml1], dx
    shr  [ml2], 1
    jnz  gmul_loop_start
    
    pop  dx ; return value
    pop  bp
    ret  4
endp gmul

proc pgmul
    ; Get two polynomials with coefficients in GF(2^8), represented as vectors of 4 bytes, and
    ; multiply them according to the corresponding rules.
    ; This is the *modular* product, as the regular one can't be kept in 4 bytes. It's important to
    ; mention that this can be represented as a form of matrix multiplication.
    ;
    ; Ex: d = a * b => [d_0, d_1, d_2, d_3] = [a_0, a_1, a_2, a_3] * [b_0, b_1, b_2, b_3]
    ;     d_0 = a_0 * b_0 + a_3 * b_1 + a_2 * b_2 + a_1 * b_3
    ;     d_1 = a_1 * b_0 + a_0 * b_1 + a_3 * b_2 + a_2 * b_3
    ;     d_2 = a_2 * b_0 + a_1 * b_1 + a_0 * b_2 + a_3 * b_3
    ;     d_3 = a_3 * b_0 + a_2 * b_1 + a_1 * b_2 + a_0 * b_3
    ; 
    ; Note: While it may be possible to implement this with a loop, I believe it is more efficient
    ; like this. Less overhead, and no need to deal with the rotating nature of the circulant matrix.
    ;
    ; Input
    ;   pol1 - pointer to the beginning of the first polynomial
    ;   pol2 - pointer to the beginning of the second polynomial
    ;   res  - pointer to the beginning of the resulting polynomial
    ; Output
    ;   res  - the resulting polynomial will be saved in the byte vector starting at res.
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   None

    arg   res,pol2,pol1
    push  bp
    mov   bp, sp
    push  bx dx si di

    xor   dx, dx
    mov   bx, [res]
    mov   [dword ptr bx], 0
    mov   si, [pol1] ; a
    mov   di, [pol2] ; b

    ; d_0
    clcpl [si] [si+3] [si+2] [si+1]

    ; d_1
    inc   bx
    clcpl [si+1] [si] [si+3] [si+2]

    ; d_2
    inc   bx
    clcpl [si+2] [si+1] [si] [si+3]

    ; d_3
    inc   bx
    clcpl [si+3] [si+2] [si+1] [si]

    pop di si bx dx
    pop bp
    ret 6
endp pgmul

proc ByteSub
    ; ***NAME AND FUNCTION DESCRIBED BY THE AES SPECIFICATION***
    ;
    ; Take the state and S-Box and apply substitution to each and every byte.
    ; Box values are precalculated because I'm not writing a matrix transformation in assembly in a
    ; million years.
    ;
    ; Input
    ;   statep - pointer to the state
    ;   sboxp  - pointer to the S-Box
    ; Output
    ;   state  - the state will change according to the given S-Box
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   None

    arg  sboxp,statep
    push bp
    mov  bp, sp
    push ax bx si

    mov  bx, [statep]
    xor  ax, ax
    mov  si, Nb*Nb ; Nb * Nb is always 16

    bytesub_loop_start:
    mov  al, [byte ptr bx+si-1]
    mov  bx, [sboxp]
    xlat
    mov  bx, [statep]
    mov  [byte ptr bx+si-1], al

    dec  si
    jnz  bytesub_loop_start

    pop  si bx ax
    pop  bp
    ret  4
endp ByteSub

proc ShiftRow
    ; ***NAME AND FUNCTION DESCRIBED BY THE AES SPECIFICATION***
    ;
    ; Shift the rows of the state. With offset 0, operates normally. With offset 2, does the inverse.
    ; In regular operation, shifts the second row (row #1) 1 column to the left, then the third row
    ; (row #2) two columns to the left, then the fourth row (row #3) three columns to the left.
    ; In inverse mode, it uses the fact that Nb=4 and C1=1, C2=2, C3=3 which means Nb-C1=C3, Nb-C2=C2
    ; and Nb-C3=C1, which means that if you muck with the indexes (the state array is a matrix indexed
    ; by column, therefore incrementing/decrementing an index changes the row upon which you operate)
    ; you can make the same function do both operations.
    ;
    ; Note: Yes, this is faster than constructing a polynomial over GF(2^8) and multiplying by x. It's
    ; way less operations, doesn't need to call an entire other function, and generally can be seen as
    ; a good idea.
    ;
    ; Input
    ;   statep - pointer to the state
    ;   ofst   - either 0 or 2, determines whether operating in regular or inverse mode
    ; Output
    ;   state  - the state is changed accordingly
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   None

    arg  ofst,statep
    push bp
    mov  bp, sp
    push ax bx

    mov  bx, [statep]

    ; move second row 1 column left
    add  bx, [ofst]
    mov  al, [byte ptr bx+01h]
    mov  ah, [byte ptr bx+05h]
    mov  [byte ptr bx+01h], ah
    mov  ah, [byte ptr bx+09h]
    mov  [byte ptr bx+05h], ah
    mov  ah, [byte ptr bx+0Dh]
    mov  [byte ptr bx+09h], ah
    mov  [byte ptr bx+0Dh], al

    ; move third row 2 columns left (same as swapping)
    sub  bx, [ofst]
    mov  al, [byte ptr bx+02h]
    mov  ah, [byte ptr bx+0Ah]
    mov  [byte ptr bx+02h], ah
    mov  [byte ptr bx+0Ah], al
    mov  al, [byte ptr bx+06h]
    mov  ah, [byte ptr bx+0Eh]
    mov  [byte ptr bx+06h], ah
    mov  [byte ptr bx+0Eh], al

    ; move fourth row 3 columns left (same as moving one right)
    sub  bx, [ofst] ; because we can't have negatives in the addressing mode
    mov  al, [byte ptr bx+03h]
    mov  ah, [byte ptr bx+0Fh]
    mov  [byte ptr bx+03h], ah
    mov  ah, [byte ptr bx+0Bh]
    mov  [byte ptr bx+0Fh], ah
    mov  ah, [byte ptr bx+07h]
    mov  [byte ptr bx+0Bh], ah
    mov  [byte ptr bx+07h], al

    pop  bx ax
    pop  bp
    ret  4
endp ShiftRow

proc MixColumn
    ; ***NAME AND FUNCTION DESCRIBED BY THE AES SPECIFICATION***
    ;
    ; Mix all columns of the given state by multiplying each polynomial with the given polynomial.
    ; I hate this implementation but I can't think of any better way to do this except for intermediate
    ; steps. It might be possible to make pgmul give a result on edx just to save up on a buffer, but
    ; that wouldn't be any saving grace for this mess.
    ;
    ; Input
    ;   statep - pointer to the state
    ;   pol    - pointer to a polynomial represented by a vector of four bytes
    ;   inter  - pointer to an intermediate polynomial in which to store results before applying them
    ; Output
    ;   state  - the state is changed accordingly
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   None

    arg inter,pol,statep
    push bp
    mov  bp, sp
    push ax si di

    mov  si, [inter]
    mov  di, [statep]

    push di
    push [pol]
    push si
    call pgmul
    mov  eax, [dword ptr si]
    mov  [dword ptr di], eax

    add  di, 4
    push di
    push [pol]
    push si
    call pgmul
    mov  eax, [dword ptr si]
    mov  [dword ptr di], eax

    add  di, 4
    push di
    push [pol]
    push si
    call pgmul
    mov  eax, [dword ptr si]
    mov  [dword ptr di], eax

    add  di, 4
    push di
    push [pol]
    push si
    call pgmul
    mov  eax, [dword ptr si]
    mov  [dword ptr di], eax

    xor eax, eax
    pop di si ax
    pop bp
    ret 6
endp MixColumn

proc AddRoundKey
    ; ***NAME AND FUNCTION DESCRIBED BY THE AES SPECIFICATION***
    ;
    ; Add the round key (reminder: adding in GF(2^8) is XORing) to each byte of the state.
    ; There's not a lot else to say about this.
    ;
    ; Input
    ;   statep - pointer to the state
    ;   rkeyp  - pointere to the beginning of the round key
    ; Output
    ;   state  - the state is changed accordingly
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   None
    
    arg  rkeyp,statep
    push bp
    mov  bp, sp
    push ax bx si di

    xor  ah, ah
    mov  si, [statep]
    mov  di, [rkeyp]
    mov  bx, Nb*Nb

    addroundkey_loop_start:
    mov  al, [byte ptr si+bx-1]
    xor  al, [byte ptr di+bx-1]
    mov  [byte ptr si+bx-1], al
    
    dec  bx
    jnz  addroundkey_loop_start

    pop  di si bx ax
    pop  bp
    ret  4
endp AddRoundKey

proc KeyExpansion
    ; ***NAME AND FUNCTION DESCRIBED BY THE AES SPECIFICATION***
    ;
    ; Expand the Cipher Key (given by the user) into the Expanded Key, which can then be cut up into
    ; (Nr+1) Round Keys. This algorithm was written in C in the spec, so wish me luck.
    ; Round keys 0 through Nk-1 are copied from the Cipher Key, all later ones are derived recursively.
    ;
    ; Input
    ;   ckeyp - pointer to the Cipher Key (Nb*Nk bytes long)
    ;   ekeyp - pointer to the Expanded Key (4*Nb*(Nr+1) bytes long)
    ;   sboxp - pointer to the S-Box (required for Key Expansion)
    ;   rconp - pointer to the Round Constant (RCon) array
    ; Output
    ;   ekey  - the Expanded Key will be written to the buffer pointed to by ekeyp
    ; Calling Convention
    ;   Pass Left to Right
    ; Registers Modified
    ;   None
    
    arg  rconp,sboxp,ekeyp,ckeyp
    push bp
    mov  bp, sp
    push ax bx cx dx si di

    xor  edx, edx
    mov  bx, 4*Nk
    mov  cl, 4*Nk
    mov  si, [ckeyp]
    mov  di, [ekeyp]
    
    keyexpansion_copy_loop:
    mov al, [byte ptr si+bx-1]
    mov [byte ptr di+bx-1], al
    dec  bx
    jnz  keyexpansion_copy_loop

    mov  bx, 4*Nk

    start_keyexpansion_xor:
    mov  edx, [dword ptr di+bx-4]
    mov  ax, bx
    div  cl
    cmp  ah, 0
    je   after_cycle
if VER EQ 256
    cmp   ah, 16
    jne   end_keyexpansion_xor
    ; Yes, this is code-copying. No, I don't care.
    mov  eax, edx
    mov  dx, bx
    mov  bx, [sboxp]
    xlat
    ror  eax, 8
    xlat
    ror  eax, 8
    xlat
    ror  eax, 8
    xlat
    ror  eax, 8
    mov  bx, dx
    mov  edx, eax
endif
    jmp end_keyexpansion_xor

    after_cycle:
    ; Basically, we're doing SubWord and RotWord at the same time.
    ; Rotating 3 times right is the same as rotating one time left. BUT this is in a register, so we
    ; rotate in the opposite direction.
    mov  ch, al
    mov  eax, edx
    mov  dx, bx
    mov  bx, [sboxp]
    xlat
    rol  eax, 8
    xlat
    rol  eax, 8
    xlat
    rol  eax, 8
    xlat
    ; Now xor by Rcon[i/Nk]
    xor  bh, bh
    mov  bl, ch
    mov  si, [rconp]
    xor  al, [byte ptr si+bx]
    mov  bx, dx
    mov  edx, eax

    end_keyexpansion_xor:
    xor  edx, [dword ptr di+bx-4*Nk]
    mov  [dword ptr di+bx], edx
    add  bx, 4
    cmp  bx, 4*Nb*(Nr+1)
    jne  start_keyexpansion_xor

    pop  di si dx cx bx ax
    pop  bp
    ret  8
endp KeyExpansion

end
