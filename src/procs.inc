; procs.inc
; Global definitions, allows for separation of modules. Useful for all procedural files.

; Procedures
global ByteSub:proc
global ShiftRow:proc
global MixColumn:proc
global AddRoundKey:proc
global KeyExpansion:proc

; Values
Nb = 4 ; Always true for AES, which has a block size of 128 bits.

; The following are useful for interrupt macros
GET_CHAR    = 01h
PRINT_STR   = 09h
GET_STR     = 0Ah
OPEN_FILE   = 3Dh
CREATE_FILE = 3Ch
CLOSE_FILE  = 3Eh
READ_FILE   = 3Fh
WRITE_FILE  = 40h
DELETE_FILE = 41h
SEEK_FILE   = 42h
; File opening modes
READ        = 0
WRITE       = 1
READWRITE   = 2
; vim:ft=tasm
