# Methods and operations in the Galois Field 2^8

MOD = 0x11B  # Pre-defined in the AES spec


def xtime(a: int) -> int:
    # Multiply a polynomial by x
    a <<= 1
    if a & 0x100:
        a ^= MOD
    return a


def gmul(a: int, b: int) -> int:
    # Multiply two polynomials
    ret = 0
    while b:
        if b & 1:
            ret ^= a
        a = xtime(a)
        b >>= 1
    return ret


def xdiv(a: int) -> int:
    # Divide a polynomial by x
    if a & 1:
        a ^= MOD
    return a >> 1


# Find the multiplicative inverse of the polynomial
def invr(a: int) -> int:
    # A^{-1} = A^254 because then A^254 * A = A^255 and in GF(2^8) the order of the multiplicative
    # subgroup is 2^8 - 1 = 255.
    b =    gmul(a, a)  #   2
    c =    gmul(a, b)  #   3
    b =    gmul(c, c)  #   6
    b =    gmul(b, b)  #  12
    c =    gmul(b, c)  #  15
    b =    gmul(b, b)  #  24
    b =    gmul(b, b)  #  48
    b =    gmul(b, c)  #  63
    b =    gmul(b, b)  # 126
    b =    gmul(a, b)  # 127
    return gmul(b, b)  # 254
