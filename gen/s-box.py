# s-box.py
# Generate the S-box required for AES, per the rules of GF(2^8).
from functools import reduce
from operator import xor
from typing import List

import numpy as np
from numpy.typing import ArrayLike

from galois import gmul

BOX_SIZE = 256  # GF(2^8) is a finite field with 256 options


def gen_inverse():
    # Generate a list of multiplicative inverses.
    # 3 is a cyclic generator on GF(2^8), which means that if you take it and its
    # multiplicative inverse you can traverse the entire field AND know the inverse of
    # every number.
    res = np.empty((BOX_SIZE, 1), dtype=np.uint8)
    i = 1
    j = 1
    while True:
        res[i][0] = j
        i = gmul(i, 0x03)
        if i == j:
            break
        j = gmul(j, 0xf6)
        res[j][0] = i
    res[0] = 0
    return res


AFFINE_MAT = np.array([[1, 0, 0, 0, 1, 1, 1, 1],
                       [1, 1, 0, 0, 0, 1, 1, 1],
                       [1, 1, 1, 0, 0, 0, 1, 1],
                       [1, 1, 1, 1, 0, 0, 0, 1],
                       [1, 1, 1, 1, 1, 0, 0, 0],
                       [0, 1, 1, 1, 1, 1, 0, 0],
                       [0, 0, 1, 1, 1, 1, 1, 0],
                       [0, 0, 0, 1, 1, 1, 1, 1]], dtype=np.uint8)

AFFINE_ADD = 0b01100011


def gen_sbox() -> ArrayLike:
    # Generate the S-box for AES
    # For any number, find its multiplicative inverse, then apply an affine map.
    ret = np.unpackbits(gen_inverse(), axis=1)
    for i in range(BOX_SIZE):
        ret[i] = ret[i] @ AFFINE_MAT % 2
        # Addition in GF(2^8) is XOR, but XOR is just addition mod 2, and modulo is defined via
        # divison, and division is associative on the nominator $\sum_n{\frac{n}{m}} = \frac{\sum_n{n}}{m}$
        # therefore XORing is the same as summing then calculating mod 2.
    ret = np.packbits(ret, axis=1)
    ret ^= AFFINE_ADD
    return ret.reshape((BOX_SIZE,))


def gen_rsbox() -> ArrayLike:
    # Generate the inverse S-box for AES
    # For any number, reverse the affine map then find the multiplicative inverse.
    tr = gen_inverse()
    ret = np.unpackbits(np.array(range(BOX_SIZE), dtype=np.uint8).reshape((BOX_SIZE, 1)) ^ AFFINE_ADD, axis=1)
    for i in range(BOX_SIZE):
        # M^4 = I, therefore M^-1 = M^3 because M^1*M^(-1) = I.
        # This has been proven via calculation, opening the file interactively and doing
        # (AFFINE_MAT @ AFFINE_MAT @ AFFINE_MAT @AFFINE_MAT) % 2
        # yields the identity matrix.
        ret[i] = ret[i] @ (AFFINE_MAT @ AFFINE_MAT @ AFFINE_MAT) % 2
    ret = np.packbits(ret, axis=1)
    for i in range(BOX_SIZE):
        ret[i] = tr[ret[i]][0]
    return ret.reshape((BOX_SIZE,))


if __name__ == "__main__":
    s = gen_sbox()
    r = gen_rsbox()
    print(", ".join(f"{hex(e)[2:].zfill(3)}h" for e in s))
    print()
    print(", ".join(f"{hex(e)[2:].zfill(3)}h" for e in r))
